import fasttext as ft
import os

'''
this script train a sentiment analysis classifier based on amazon polarity dataset
the output is a binary file that could be used to predict the sentiment of a news
'''


current_dir = os.path.dirname(__file__)
prepareData_dir = "../data/sentiment/amazon_review_polarity_csv/"
input_file = os.path.join(prepareData_dir, 'output-train.csv-corrected.csv')
output = 'bin/classifier-sentiment'


# set params
dim=100
lr=0.1
epoch=25
min_count=2
word_ngrams=2
bucket=2000000
thread=4
silent=1
label_prefix='__label__'

# Train the classifier
classifier = ft.supervised(input_file, output, dim=dim, lr=lr, epoch=epoch,
    min_count=min_count, word_ngrams=word_ngrams, bucket=bucket,
    thread=thread, silent=silent, label_prefix=label_prefix)

