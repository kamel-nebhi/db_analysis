import fasttext as ft
import csv
import pandas as pd

'''
this script annotate each article with as positive or negative
'''


classifier = ft.load_model('bin/classifier-sentiment.bin', encoding='utf-8')

df = pd.read_csv("classes-map.csv", delimiter=",", encoding="utf8")
feat_labels = df.columns[0:]
df.set_index('label', inplace=True)
#print(feat_labels)

inputFile = "../data/output.csv"
df2 = pd.read_csv(inputFile, delimiter=",", encoding="utf8", quoting=csv.QUOTE_ALL)

doc = []
with open("../data/output-sentiment.csv", 'w') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_NONE, delimiter='|')
    wr.writerow(["class", "proba_score", "headline"])

    for index, row in df2.iterrows():
        doc.append(row['article'])
        cat = classifier.predict_proba(doc)[0][0][0]
        proba = classifier.predict_proba(doc)[0][0][1]
        mapping = df["Category"].iloc[df.index.get_indexer([cat])].values[0]
        print(mapping, "==>", proba, "==>", row['headline'])
        doc = []
        wr.writerow([mapping, proba, row['headline']])

myfile.close()

