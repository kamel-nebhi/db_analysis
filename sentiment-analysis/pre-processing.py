import pandas as pd
import nltk
import re
import os
import string
import csv

'''
this script take the original file as an input and provide the right formatting to feed fastText ML classifier
the dataset for training is available here:

'''


def remove_punctuation(s):
    s = ''.join([i for i in s if i not in frozenset(string.punctuation)])
    return s

inputFile = "../data/sentiment/amazon_review_polarity_csv/train-sample.csv"
outputFile = "../data/sentiment/amazon_review_polarity_csv/output-train.csv"
outputFileCorrected = "../data/sentiment/amazon_review_polarity_csv/output-train.csv-corrected.csv"

df = pd.read_csv(inputFile, delimiter=",", encoding="utf8",names=["Category",'Title', 'Descript'],quoting=csv.QUOTE_ALL)

#feat_labels = df.columns[0:]

df['label'] = df['Category'].astype('category')

cat_columns = df.select_dtypes(['category']).columns


df[cat_columns] = '__label__' + df[cat_columns].apply(lambda x: x.cat.codes).astype(str)

df['tokenized_sents'] =  df.apply(lambda row: ' '.join(nltk.word_tokenize(row['Descript'])), axis=1)
df['tokenized_sents'].replace({'"': ''}, regex=True)
df['tokenized_sents'].str.lower()

df['tokenized_sents'] = df['tokenized_sents'].apply(remove_punctuation)

# GENERATE CLASSES MAP
df_classes = df.groupby(["label", "Category"]).size()
classes = df_classes.to_csv("classes-map.csv", encoding="utf8", header=True)

test = df.to_csv(outputFile, encoding="utf8", columns=["label","tokenized_sents"], header=False, index=False)

with open(outputFile) as infile, open(outputFileCorrected, 'w') as outfile:
    for line in infile:
        line = re.sub(r"(__label__[0-9]{1,3})(,)", r"\1 , ", line)
            #line = line.replace(src, target)
        outfile.write(line)

# suppress the temp file
os.remove(outputFile)


