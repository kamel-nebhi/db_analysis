# DB analysis

## data pre-preprocessing

This directory provides a script which allows to transform and split the xml file into a csv file.
This csv file is readable with pandas.

## ner-analysis

This directory provides a script which detect and extract NE in the news dataset.
The output is in the data directory in different csv files that contains the NE and the frequency for
each kind of NE (PERSON, LOCATION, ORG).

## sentiment analysis

This directory provides different script based on the fastText approach for text classification.
We have trained a model to find the sentiment analysis class (positive or negative).

The output is in the data directory and it provides the label (+ or -), the probability score and
the headline of the article.

