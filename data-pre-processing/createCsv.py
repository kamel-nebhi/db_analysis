from bs4 import BeautifulSoup
import pandas as pd
import csv
import html

'''
this script create split and transform the xml file in a csv file 
'''

soup = BeautifulSoup(open("../data/sample_news.xml"), "html.parser")

with open('data/output.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_ALL)
    spamwriter.writerow(["headline", "article"])

    for content in soup.findAll('body'):
        hd = content.findAll('headline')
        hd_text = hd[0].text
        article = content.findAll('text')
        article_text = article[0].text
        spamwriter.writerow([html.unescape(hd_text), html.unescape(article_text)])


df = pd.read_csv('../data/output.csv')
features_list = df.columns[0:]

print(df.head())

